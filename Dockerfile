FROM python:3.12-slim

WORKDIR /app

COPY requirements.txt .

RUN apt update
RUN apt install libpq-dev gcc -y

RUN pip install -r requirements.txt
RUN pip install gunicorn

EXPOSE 8000

# When running the container you must mount the directory cloned from gitlab as the /app directory of this container
# For example, if you are running the container inside the cloned dir, use this command (<host>:<container>):
# docker run --name gunicorn-bugout -v .:/app -p 9001:8000 -d --restart always gunicorn-bugout

CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--keyfile", "key.pem", "--certfile", "cert.pem", "bugout.wsgi:application"]
