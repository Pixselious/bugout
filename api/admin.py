from django.contrib import admin


from .models import Article, ListItem, Option

# Register your models here.


class MyAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)


admin.site.register(Option, MyAdmin)
admin.site.register(ListItem, MyAdmin)
admin.site.register(Article, MyAdmin)
