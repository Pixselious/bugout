# Generated by Django 4.1.3 on 2022-12-15 13:46

from django.db import migrations, models
import django_resized.forms


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Option',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nameEN', models.TextField()),
                ('nameRU', models.TextField()),
                ('icon', django_resized.forms.ResizedImageField(blank=True, crop=None, force_format='WEBP', keep_meta=False, quality=85, scale=None, size=[1920, 1080], upload_to='')),
                ('photo', django_resized.forms.ResizedImageField(blank=True, crop=None, force_format='WEBP', keep_meta=False, quality=85, scale=None, size=[1920, 1080], upload_to='')),
                ('descriptionEN', models.TextField(blank=True)),
                ('descriptionRU', models.TextField(blank=True)),
                ('advantageEN', models.TextField(blank=True)),
                ('advantageRU', models.TextField(blank=True)),
                ('tradeoffEN', models.TextField(blank=True)),
                ('tradeoffRU', models.TextField(blank=True)),
                ('weight', models.FloatField()),
                ('climate', models.CharField(choices=[('H', 'Hot'), ('C', 'Cold'), ('A', 'Any')], default='A', max_length=1)),
                ('days', models.IntegerField()),
                ('priority', models.SmallIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='ListItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.CharField(choices=[('FW', 'Food and water'), ('HFA', 'Health and first aid'), ('CE', 'Comms and electricity')], max_length=5)),
                ('nameEN', models.TextField()),
                ('nameRU', models.TextField()),
                ('priority', models.SmallIntegerField()),
                ('optional', models.BooleanField(default=False)),
                ('options', models.ManyToManyField(to='api.option')),
            ],
        ),
    ]
