from os import chmod
from queue import Empty
from django.db import models
from django_resized import ResizedImageField
from django_ckeditor_5.fields import CKEditor5Field


# Create your models here.


class Option(models.Model):

    name = models.TextField()

    weight = models.FloatField()
    climate = models.CharField(max_length=1, choices=[('H', 'Hot'), ('C', 'Cold'), ('A', 'Any')], default='A')
    days = models.IntegerField()

    priority = models.SmallIntegerField()

    def __str__(self):
        return (self.climate + " | " + str(self.days) + " | " + self.name)

    class Meta:
        ordering = ('priority', )




class ListItem(models.Model):

    """
    MUST MATCH FRONTEND

    ListGeneratorPage.js

        const categories = [
        { name: 'Shelter and clothing', short: 'SAC' },
        { name: 'Food and water', short: 'FAW' },
        { name: 'Temperature', short: 'TMP' },
        { name: 'Hygiene and first aid', short: 'HFA' },
        { name: 'Information and electronics', short: 'IAE' },
        { name: 'Other gear', short: 'OTH' },
        { name: 'Optional', short: 'OPT' },
    ]

    """



    category = models.CharField(max_length=5, choices=[
        ('SAC', 'Shelter and clothing'),
        ('FAW', 'Food and water'),
        ('TMP', 'Temperature'),
        ('HFA', 'Hygiene and first aid'),
        ('IAE', 'Information and electronics'),
        ('OTH', 'Other gear'),
        ('ADD', 'Add-ons'),
    ])

    name = models.TextField()
    priority = models.SmallIntegerField()

    optional = models.BooleanField(default=False)
    defaultSelected = models.BooleanField(default=False)

    options = models.ManyToManyField(to=Option, blank=True)

    def __str__(self):
        return self.name

class Article(models.Model):
    titleEN = models.TextField()
    titleRU = models.TextField()
    priority = models.IntegerField()
    difficulty = models.CharField(max_length=3)
    contentEN = CKEditor5Field(config_name='extends')
    contentRU = CKEditor5Field(config_name='extends')

    def __str__(self):
        return (self.titleEN + '|' + str(self.priority))

    class Meta:
        ordering = ('priority', )
