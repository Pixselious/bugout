from rest_framework import serializers
from .models import Article, ListItem, Option

class OptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Option
        fields = '__all__'


class ListSerializer(serializers.ModelSerializer):

    #listitem = serializers.TextField(source='nameEN', read_only=True)
    options = OptionSerializer(many=True)

    class Meta:
        model = ListItem
        fields = '__all__'

class ArticleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        fields = '__all__'
