from django.urls import path
from .views import GetArticles, GetArticle, GetList

urlpatterns = [
    path('get-list', GetList.as_view()),
    path('get-articles', GetArticles.as_view()),
    path('get-articles/<int:article_id>/', GetArticle.as_view(), name='get_article'),
]
