from rest_framework.views import APIView
from rest_framework import generics, status
from rest_framework.response import Response
from .serializers import ListSerializer, ArticleSerializer
from .models import ListItem, Article

# Create your views here.

class GetList(APIView):
    serializer_class = ListSerializer

    def get(self, request, format=None):
        payload = ListSerializer(ListItem.objects.all(), many= True).data
        return Response(payload, status=status.HTTP_200_OK)


class GetArticles(APIView):
    serializer_class = ArticleSerializer

    def get(self, request, format=None):
        payload = ArticleSerializer(Article.objects.all(), many= True).data
        return Response(payload, status=status.HTTP_200_OK)

class GetArticle(APIView):
    serializer_class = ArticleSerializer

    def get(self, request, article_id, format=None):
        try:
            article = Article.objects.get(pk=article_id)
            serializer = self.serializer_class(article)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Article.DoesNotExist:
            return Response({"error": "Article does not exist"}, status=status.HTTP_404_NOT_FOUND)
