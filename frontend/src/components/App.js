import React, { Component, useState, useEffect } from "react";
import { createRoot } from 'react-dom/client';
import { IntlProvider } from 'react-intl';
import Router from "./Router"
import './App.scss';


import messagesEN from "/src/lang/en.json"


// Internationalization



let initLocale = "en";

const supportedLanguages = ['en', 'ru', 'es'];

const browserLanguage = navigator.language;

if(supportedLanguages.includes(browserLanguage)){
  initLocale = browserLanguage;
} else {
  initLocale = 'en';
}



var loaded = false

export default function App(props) {

    const [locale, setLocale] = useState(initLocale);
    const [messages, setMessages] = useState(messagesEN);

    function changeLocale(newLocale) {
        setLocale(newLocale);
        initLocale = newLocale;

        loaded = false;
    }

    useEffect( () =>{

    async function loadMessages(locale) {

      var initMessages = import(`/src/lang/${locale}.json`);

      if (locale != 'en') {
          initMessages = import(`/src/lang/${locale}.json`);
      } else {
          initMessages = messagesEN 
      }

      setMessages(await initMessages);
    }
 
    if (loaded == false){
      loadMessages(locale);
      loaded = true;
    };

  });
   
    return(
        <IntlProvider locale={locale} messages={messages}>
            <Router locale={locale} changeLocale={changeLocale} />
        </IntlProvider>
    );

}

const appDiv = document.getElementById('app'); 
const root = createRoot(appDiv);
root.render(<App tab="home" />);