import React, { useState, useEffect } from "react";
import { Container, Card } from 'react-bootstrap';
import { useParams } from "react-router-dom";
import { useIntl } from "react-intl";

export default function ArticlePage({ locale }) {
  const { articleID } = useParams();
  const [article, setArticle] = useState(null);

  useEffect(() => {
    function getArticles() {
      fetch(`/api/get-articles/${articleID}`)
        .then(response => response.json())
        .then(data => {
          setArticle(data);
        })
        .catch(error => console.error("Error fetching articles:", error));
    }

    if (!article) {
      getArticles();
    }
  }, [article, locale]);

  return (
    <Container>
      {article && (
        <Card className="rounded rounded-5 border-0 shadow my-3 mx-1" style={{backgroundColor: 'white', overflow: 'hidden'}}>
          <Card.Body>
            <Card.Title>
              {(locale === 'es' ? article['titleEN'] :
                (locale === 'ru' ? article['titleRU'] :
                  article['titleEN'])
              )}
            </Card.Title>
            <div dangerouslySetInnerHTML={{ __html:
              (locale === 'es' ? article['contentEN'] :
                (locale === 'ru' ? article['contentRU'] :
                  article['contentEN'])
              )}} />
          </Card.Body>
        </Card>

      )}
    </Container>
  );
}
