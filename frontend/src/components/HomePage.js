import React, { useEffect, useState, useMemo, useRef } from "react";
import { Button, Form, Card, Container, Row, Col, Stack, Image, Modal } from 'react-bootstrap';
import { FormattedMessage, useIntl } from 'react-intl';
import { useNavigate } from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAnglesUp, faArrowDownShortWide, faBolt, faBrain, faBuilding, faCampground, faChevronRight, faClock, faCloudShowersHeavy, faFilm, faHeartCrack, faHelmetSafety, faHouseCrack, faListCheck, faListUl, faLocationDot, faLock, faMapLocationDot, faPaw, faPersonCane, faRectangleList, faShapes, faShield, faTent, faUserCheck } from '@fortawesome/free-solid-svg-icons'

import { Translate, TelephoneOutboundFill, Incognito } from 'react-bootstrap-icons';

import useGeoLocation from "react-ipgeolocation";



export default function HomePage({changeLocale, locale}) {

  const location = useGeoLocation();
  let located = false;

  const divRef = useRef(null);

  const scrollToDiv = () => {
      divRef.current.scrollIntoView({ behavior: 'auto' });
    };


  const [articles, setArticles] = useState(null);

  const [filter, setFilter] = useState('all');

  const filteredArticles = useMemo(() => {
          if (!articles) return [];
          if (filter === 'all') {
              return articles;
          } else {
              return articles.filter(article => article.difficulty === filter);
          }
      }, [articles, filter]);


  let navigate = useNavigate();
  const routeChange = () =>{
    let path = `/generate`;
    navigate(path);
  };

  var initLocale = locale

  const intl = useIntl();
  const supportedCountries = ["US", "CA", "GB", "AU", "IN", "DE", "FI", "NL", "RU", "BG", "KZ", "ES", "MX", "AR", "CL", "RO", "BE", "GR", "PT", "SE", "FR", "IT", "PL"]
  // don't forget to update form.select on the black card

  const [country, setCountry] = useState("null");
  const handleCountryChange = (e) => setCountry(e.target.value);

  function toJson(data) {
      return data.json();
  };

  useEffect(() => {

    function getArticles() {
        fetch('/api/get-articles')
                .then( (response) => toJson(response) )
                .then( (formatted) => {
                    setArticles(formatted)
                }).catch((e) => console.log(e))
    };

    if(articles == null){
        getArticles();
    };

    if (location.isLoading == false && country == "null" && located == false) {
      located = true;
      if (supportedCountries.includes(location.country)){
        setCountry(location.country);
      } else {
        setCountry('--');
      }
    }

  })




  const [heroActive, setHeroActive] = useState(false);

  const [showLanguageModal, setShowLanguageModal] = useState(false);

  function handleLocaleChange(newLocale){
    changeLocale(newLocale);
    setShowLanguageModal(false);
  }


  return( <div>

      <Card className="border-0 rounded-0 text-center shadow" style={{backgroundColor: "rgb(0, 20, 0)"}}>
        <Card.Img src={`${static_url}images/background-30.webp`} className="rounded-0" style={{height: "100vh", objectFit: "cover", opacity: "0.3", filter:"blur(2px)"}}/>
        <Card.ImgOverlay className="d-flex" style={{height: "100vh"}}>

          <div className="m-auto">
            <Stack direction="vertical">

              <h1 className="text-nowrap fw-bold my-0" style={{fontSize: "14vw", color: "rgb(220, 255, 220)"}}><FormattedMessage id="home.hero" defaultMessage={"GET READY"} /></h1>

              <div style={{height: '1vh'}}/>

              <h4 className="fw-light" style={{color: 'white', opacity: "0.9"}}><FormattedMessage id="home.tagline.1" defaultMessage={"Answer 3 simple questions and get a "} /><strong className="fw-bold"><FormattedMessage id="home.tagline.bold" defaultMessage={"personalized "}/></strong> <FormattedMessage id="home.tagline.2" defaultMessage={"grab-and-go bag packing list"}/></h4>

              <div style={{height: '3vh'}}/>

              <Button onClick={routeChange} className="btn btn-light rounded-pill p-3 px-4 mx-auto">
                <FormattedMessage id="home.button" defaultMessage={"Get a list"}/>
              </Button>

            </Stack>



          </div>
          { window.innerHeight >= 666 &&

            <div className="position-absolute bottom-0 start-50 translate-middle-x mb-0" style={{color: "rgb(150, 200, 150)", paddingBottom: "60px"}}>

              <h6 className="" style={{color: "rgb(150, 200, 150)"}}><FormattedMessage id="home.arrow" defaultMessage={"Or learn how to make an emergency plan"}/></h6>

              <div className="line-container mx-auto my-3">
                  <div className="line"></div>
              </div>

            </div>

          }

          { window.innerHeight < 666 &&

            <div className="position-absolute bottom-0 start-50 translate-middle-x mb-0" style={{color: "rgb(150, 200, 150)"}}>

              { window.innerHeight >= 530 &&

                <h6 className="" style={{color: "rgb(150, 200, 150)"}}><FormattedMessage id="home.arrow" defaultMessage={"Or learn how to make an emergency plan"}/></h6>

              }

              <div className="line-container mx-auto my-3">
                  <div className="line"></div>
              </div>

            </div>

          }

          <Button onClick={() => setShowLanguageModal(!showLanguageModal)} variant="outline-light" className="m-3 position-absolute top-0 start-0" style={{height: 30, width: 30, opacity: 0.5}}>
              <Translate className="position-absolute top-50 start-50 translate-middle"/>
          </Button>

          <Modal size="sm" show={showLanguageModal} onHide={() => setShowLanguageModal(false)} centered>
            <Modal.Body>

              {/*<CloseButton className="p-3 position-absolute top-0 end-0" onClick={() => setShowLanguageModal(false)} />*/}

              <>

                <Stack className="mx-auto" direction="vertical" gap={3}>

                  <Button onClick={() => handleLocaleChange('en')}>
                    English
                  </Button>
                  <Button onClick={() => handleLocaleChange('ru')}>
                    Русский
                  </Button>
                  <Button onClick={() => handleLocaleChange('es')}>
                    Español (IA)
                  </Button>
                  <a target="_blank" href="https://poeditor.com/join/project/4imMWlHu6l" className="mx-auto" style={{color: "black"}}><small>
                    <FormattedMessage id="home.language" defaultMessage="Help translate this website"/>
                  </small></a>

                </Stack>

              </>

            </Modal.Body>
          </Modal>

        </Card.ImgOverlay>
      </Card>

      <div className="bg-dark" style={{minHeight: "1500px"}} >
      <Container className="pt-5 py-3 py-md-5">

        <>
          <Card className="rounded rounded-5 border-0 shadow m-0 p-0" style={heroActive ? { minHeight: '50vh', backgroundColor: 'white', overflow: 'hidden'} : {minHeight: '50vh', backgroundColor: 'white', overflow: 'hidden'}}>
                <Card.Body className="p-0">

                    <Card.Title className="m-3 m-md-4 mb-5" style={{color: 'black'}}><FormattedMessage id="home.white.title" /></Card.Title>

                    <Row className="row-cols-1 row-cols-lg-3 justify-content-evenly align-items-center" style={{minHeight: '45vh'}}>

                        <Col>
                        <div className="p-0 m-0" style={{position: 'relative', height: '20rem'}}>

                          <h1 className="fw-bold" style={{color: '#ff7da8', fontSize: "20rem", position: 'absolute', top: -90, left: -25}}>1</h1>

                          <p className="me-3" style={{color: 'black', textShadow: '0px 0px 8px white', minWidth: '16rem', maxWidth: '19rem', position: 'absolute', top: 65, left: 30}}><FormattedMessage id="home.white.1" /></p>

                        </div>
                        </Col>

                        <Col>
                        <div style={{position: 'relative', height: '20rem'}}>

                          <h1 className="fw-bold" style={{color: '#ff7d7d', fontSize: "20rem", position: 'absolute', top: -90, left: -25}}>2</h1>

                          <p className="me-3" style={{color: 'black', textShadow: '0px 0px 8px white', minWidth: '16rem', maxWidth: '19rem', position: 'absolute', top: 65, left: 30}}><FormattedMessage id="home.white.2" /></p>

                        </div>
                        </Col>

                        <Col>
                        <div style={{position: 'relative', height: '20rem'}}>

                          <h1 className="fw-bold" style={{color: '#ffaf7d', fontSize: "20rem", position: 'absolute', top: -90, left: -25}}>3</h1>

                          <p className="me-3" style={{color: 'black', textShadow: '0px 0px 8px white', minWidth: '16rem', maxWidth: '19rem', position: 'absolute', top: 65, left: 30}}><FormattedMessage id="home.white.3" /></p>

                        </div>
                        </Col>

                    </Row>

                    <small className="position-absolute bottom-0 start-50 translate-middle-x mb-2 text-center" style={{color: 'lightgray', minWidth: '16rem'}}><FormattedMessage id="home.white.credit.1" /><a className="text-reset" target="_blank" href="https://www.redcross.org/get-help/how-to-prepare-for-emergencies/make-a-plan.html"><FormattedMessage id="home.white.credit.link" /></a><FormattedMessage id="home.white.credit.2" /></small>

                </Card.Body>

                { /* Expand button <a className="m-3 position-absolute bottom-0 end-0" onClick={() => setHeroActive(!heroActive)}> <Icon.PlusCircleFill fill="gray" fillOpacity={0.5} size={"40px"}/> </a> */ }

          </Card>

          <Card className="rounded rounded-5 border-0 shadow m-0  mt-3" style={{minHeight: '20vh', backgroundColor: 'white', overflow: 'hidden'}}>
                <Card.Body className="p-0">

                    <Card.Title className="m-3 m-md-4 mb-5" style={{color: 'black'}}><FormattedMessage id="home.faq.title" /></Card.Title>

                     <Row className="row-cols-1 row-cols-lg-3 justify-content-evenly align-items-center text-lg-center p-5  m-lg-5" style={{minHeight: '20vh'}}>
                        <Col className="py-3">
                            <h3 className="btn fs-3 fw-medium rounded-4 text-decoration-underline" onClick={() => navigate(`/article/${articles[0].id}`)}>{articles ?
                              (locale === 'es' ? articles[0].titleEN :
                                (locale === 'ru' ? articles[0].titleRU :
                                  articles[0].titleEN)
                              )
                              : 'Loading'} </h3>
                        </Col>
                        <Col className="py-3">
                          <h3 className="btn fs-3 fw-medium rounded-4 text-decoration-underline" onClick={() => navigate(`/article/${articles[1].id}`)}>{articles ?
                            (locale === 'es' ? articles[1].titleEN :
                              (locale === 'ru' ? articles[1].titleRU :
                                articles[1].titleEN)
                            )
                            : 'Loading'}</h3>
                        </Col>
                        <Col className="py-3">
                          <h3 className="btn fs-3 fw-medium rounded-4 text-decoration-underline" onClick={() => navigate(`/article/${articles[2].id}`)}>{articles ?
                            (locale === 'es' ? articles[2].titleEN :
                              (locale === 'ru' ? articles[2].titleRU :
                                articles[2].titleEN)
                            )
                            : 'Loading'}</h3>
                        </Col>
                     </Row>

                </Card.Body>

                <Button variant="outline-primary m-3 ms-auto rounded-4" onClick={scrollToDiv} ><FormattedMessage id="home.faq.button" /> <FontAwesomeIcon icon={faChevronRight} className=""/></Button>{' '}

          </Card>

          <Row>

              <Col md={12} lg={4} xl={5} className="mt-3 pe-3 pe-lg-2">

                <Card className="rounded rounded-5 border-0 shadow m-0 p-0" style={{ minHeight: '600px', color: 'white', background: 'linear-gradient(to bottom right, #206941, #01a741)'}}>
                  <Card.Title className="m-3 m-md-4"><FormattedMessage id="home.green.left.title" /></Card.Title>

                  <div className="mx-3 mx-md-4 mt-0 mb-3 mb-md-4">

                  <h6 className="pe-3"><FontAwesomeIcon icon={faArrowDownShortWide} className="me-2"/><FormattedMessage id="home.green.left.1.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.green.left.1.p" /></p>

                  <h6 className="pe-3"><FontAwesomeIcon icon={faAnglesUp} className="me-2"/><FormattedMessage id="home.green.left.2.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.green.left.2.p" /></p>

                  <h6 className="pe-3"><FontAwesomeIcon icon={faCloudShowersHeavy} className="me-2"/><FormattedMessage id="home.green.left.3.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.green.left.3.p" /></p>

                  <h6 className="pe-3"><Incognito className="me-2"/><FormattedMessage id="home.green.left.4.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.green.left.4.p" /></p>

                  </div>

                </Card>

              </Col>

              <Col md={12} lg={8} xl={7} className="mt-3 ps-3 ps-lg-2">

                  <Card className="rounded rounded-5 border-0 shadow m-0" style={{ minHeight: '600px', overflow: 'hidden'}}>

                    <Card.Title className="m-3 m-md-4" style={{color: '#206941'}}><FormattedMessage id="home.green.right.title" /></Card.Title>

                    <Row className="row-cols-auto row-cols-lg-2">

                      <Col className="">

                        <div className="ms-3 ms-md-4 mt-0 mb-3 mb-md-4">

                        <h6 className="pe-3"><FontAwesomeIcon icon={faLock} className=" me-2"/><FormattedMessage id="home.green.right.1.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '350px'}}><FormattedMessage id="home.green.right.1.p" /></p>

                        <h6 className="pe-3"><FontAwesomeIcon icon={faClock} className=" me-2"/><FormattedMessage id="home.green.right.2.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '350px'}}><FormattedMessage id="home.green.right.2.p" /></p>

                        <h6 className="pe-3"><FontAwesomeIcon icon={faBolt} className=" me-2"/><FormattedMessage id="home.green.right.3.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '350px'}}><FormattedMessage id="home.green.right.3.p" /></p>

                        </div>

                      </Col>

                      <Col className="pb-md-4 m-auto">

                        <Image src={`${static_url}images/iPhone_list_${ initLocale.toUpperCase() }.webp`} style={{maxHeight: '500px'}} loading="lazy"/>

                      </Col>

                    </Row>

                  </Card>

              </Col>

          </Row>

          <Row>

              <Col md={12} lg={7} className="mt-3 pe-3 pe-lg-2">

                <Card className="rounded rounded-5 border-0 shadow m-0 p-0" style={{ minHeight: '600px',}}>
                  <Card.Title className="m-3 m-md-4" style={{color: "#1e5067"}}><FormattedMessage id="home.blue.left.title" /></Card.Title>

                  <div className="mx-3 mx-md-4 mt-0 mb-3 mb-md-4">

                  <h6 className="pe-3"><FontAwesomeIcon icon={faBuilding} className=" me-2"/><FormattedMessage id="home.blue.left.1.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '570px'}}><FormattedMessage id="home.blue.left.1.p" /></p>

                  <h6 className="pe-3"><FontAwesomeIcon icon={faTent} className=" me-2"/><FormattedMessage id="home.blue.left.2.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '570px'}}><FormattedMessage id="home.blue.left.2.p" /></p>

                  <h6 className="pe-3"><FontAwesomeIcon icon={faCampground} className=" me-2"/><FormattedMessage id="home.blue.left.3.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '570px'}}><FormattedMessage id="home.blue.left.3.p" /></p>

                  </div>

                </Card>

              </Col>

              <Col md={12} lg={5} className="mt-3 ps-3 ps-lg-2">

                  <Card className="rounded rounded-5 border-0 shadow m-0" style={{ minHeight: '600px',  color: 'white', background: 'linear-gradient(to bottom right, #1e5067, #159ab7)'}}>

                    <Card.Title className="m-3 m-md-4"><FormattedMessage id="home.blue.right.title" /></Card.Title>


                        <div className="mx-3 mx-md-4 mt-0 mb-3 mb-md-4">

                        <h6 className="pe-3"><FontAwesomeIcon icon={faLocationDot} className=" me-2"/><FormattedMessage id="home.blue.right.1.h.1" /><a className="text-reset" target="_blank" href="https://organicmaps.app"><FormattedMessage id="home.blue.right.1.h.link" /></a></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.blue.right.1.p" /></p>

                        <h6 className="pe-3"><FontAwesomeIcon icon={faFilm} className=" me-2"/><FormattedMessage id="home.blue.right.2.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.blue.right.2.p" /></p>

                        <h6 className="pe-3"><FontAwesomeIcon icon={faShield} className=" me-2"/><FormattedMessage id="home.blue.right.3.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.blue.right.3.p" /></p>

                        </div>

                  </Card>

              </Col>

          </Row>

          <Row>

            <Col md={12} lg={5} className="mt-3 pe-3 pe-lg-2">

                  <Card className="rounded rounded-5 border-0 shadow m-0" style={{ minHeight: '600px',  color: 'white', background: 'linear-gradient(to bottom right, #b95110, #ff7300)'}}>

                    <Card.Title className="m-3 m-md-4"><FormattedMessage id="home.orange.left.title" /></Card.Title>


                        <div className="mx-3 mx-md-4 mt-0 mb-3 mb-md-4">

                        <h6 className="pe-3"><FontAwesomeIcon icon={faMapLocationDot} className=" me-2"/><FormattedMessage id="home.orange.left.1.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.orange.left.1.p" /></p>

                        <h6 className="pe-3"><TelephoneOutboundFill className=" me-2"/><FormattedMessage id="home.orange.left.2.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.orange.left.2.p" /></p>

                        <h6 className="pe-3"><FontAwesomeIcon icon={faUserCheck} className=" me-2"/><FormattedMessage id="home.orange.left.3.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.orange.left.3.p" /></p>

                        </div>

                  </Card>

              </Col>

              <Col md={12} lg={7} className="mt-3 ps-3 ps-lg-2">

                <Card className="rounded rounded-5 border-0 shadow m-0 p-0" style={{ minHeight: '600px',}}>
                  <Card.Title className="m-3 m-md-4" style={{color: "#b95110"}}><FormattedMessage id="home.orange.right.title" /></Card.Title>

                  <div className="mx-3 mx-md-4 mt-0 mb-3 mb-md-4">

                  <h6 className="pe-3"><FontAwesomeIcon icon={faShapes} className=" me-2"/><FormattedMessage id="home.orange.right.1.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '550px'}}><FormattedMessage id="home.orange.right.1.p" /></p>

                  <h6 className="pe-3"><FontAwesomeIcon icon={faPersonCane} className=" me-2"/><FormattedMessage id="home.orange.right.2.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '550px'}}><FormattedMessage id="home.orange.right.2.p" /></p>

                  <h6 className="pe-3"><FontAwesomeIcon icon={faPaw} className=" me-2"/><FormattedMessage id="home.orange.right.3.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '550px'}}><FormattedMessage id="home.orange.right.3.p" /></p>

                  </div>

                </Card>

              </Col>

          </Row>

          <Row>

              <Col md={12} lg={7} className="mt-3 pe-3 pe-lg-2">

                <Card className="rounded rounded-5 border-0 shadow m-0 p-0" style={{ minHeight: '600px',}}>
                  <Card.Title className="m-3 m-md-4" style={{color: "#401e67"}}><FormattedMessage id="home.purple.left.title" /></Card.Title>

                  <div className="mx-3 mx-md-4 mt-0 mb-3 mb-md-4">

                  <h6 className="pe-3"><FontAwesomeIcon icon={faRectangleList} className=" me-2"/><FormattedMessage id="home.purple.left.1.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '570px'}}><FormattedMessage id="home.purple.left.1.p" /></p>

                  <h6 className="pe-3"><FontAwesomeIcon icon={faHouseCrack} className=" me-2"/><FormattedMessage id="home.purple.left.2.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '570px'}}><FormattedMessage id="home.purple.left.2.p" /></p>

                  <h6 className="pe-3"><FontAwesomeIcon icon={faHeartCrack} className=" me-2"/><FormattedMessage id="home.purple.left.3.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '570px'}}><FormattedMessage id="home.purple.left.3.p" /></p>

                  <h6 className="pe-3"><FontAwesomeIcon icon={faHelmetSafety} className=" me-2"/><FormattedMessage id="home.purple.left.4.h" /></h6>
                  <p className="ps-5 pe-3" style={{maxWidth: '570px'}}><FormattedMessage id="home.purple.left.4.p" /></p>

                  </div>

                </Card>

              </Col>

              <Col md={12} lg={5} className="mt-3 ps-3 ps-lg-2">

                  <Card className="rounded rounded-5 border-0 shadow m-0" style={{ minHeight: '600px', color: 'white', background: 'linear-gradient(to bottom right, #401e67, #8415b7)'}}>

                    <Card.Title className="m-3 m-md-4"><FormattedMessage id="home.purple.right.title" /></Card.Title>


                        <div className="mx-3 mx-md-4 mt-0 mb-3 mb-md-4">

                        <h6 className="pe-3"><FontAwesomeIcon icon={faListUl} className=" me-2"/><FormattedMessage id="home.purple.right.1.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.purple.right.1.p.1" /><a className="text-reset" onClick={routeChange} href='#'><FormattedMessage id="home.purple.right.1.p.link.1" /></a><FormattedMessage id="home.purple.right.1.p.2" /><a className="text-reset" target="_blank" href="https://organicmaps.app"><FormattedMessage id="home.purple.right.1.p.link.2" /></a><FormattedMessage id="home.purple.right.1.p.3" /></p>

                        <h6 className="pe-3"><FontAwesomeIcon icon={faListCheck} className=" me-2"/><FormattedMessage id="home.purple.right.2.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.purple.right.2.p" /></p>

                        <h6 className="pe-3"><FontAwesomeIcon icon={faBrain} className=" me-2"/><FormattedMessage id="home.purple.right.3.h" /></h6>
                        <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.purple.right.3.p" /></p>

                        </div>

                  </Card>

              </Col>

          </Row>

          <Card className="rounded rounded-5 border-0 shadow mt-3 p-0" style={{ minHeight: '400px', color: 'white', background: 'linear-gradient(to bottom right, #000000, #1c1b25)'}}>

            <div className="m-3 m-md-4 mb-5">
              <Stack direction="horizontal" gap={3}>
                <h4>
                  <FormattedMessage id="home.black.title" />
                </h4>

                <Form className="m-0 p-0" style={{maxWidth: "200px"}}>
                      <Form.Select size="sm" value={country} onChange={handleCountryChange}>

                                  <option value="--">International</option>
                                  <option value="US">United States</option>
                                  <option value="CA">Canada</option>
                                  <option value="GB">United Kingdom</option>
                                  <option value="AU">Australia</option>

                                  <option value="IN">India</option>
                                  <option value="DE">Deutschland</option>
                                  <option value="FI">Suomi</option>
                                  <option value="NL">Nederland</option>
                                  <option value="RU">Россия</option>

                                  <option value="BG">България</option>
                                  <option value="KZ">Қазақстан</option>
                                  <option value="ES">España</option>
                                  <option value="MX">México</option>
                                  <option value="AR">Argentina</option>

                                  <option value="CL">Chile</option>
                                  <option value="RO">România</option>
                                  <option value="BE">Belgique</option>
                                  <option value="GR">Ελλάδα</option>
                                  <option value="PT">Portugal</option>

                                  <option value="SE">Sverige</option>
                                  <option value="FR">France</option>
                                  <option value="IT">Italia</option>
                                  <option value="PL">Polska</option>


                      </Form.Select>
                </Form>

              </Stack>
              <h6 className="mt-3">
                <FormattedMessage id={`home.black.${country}`} defaultMessage=" "/>
              </h6>
            </div>

                  <div className="mx-3 mx-md-4 mt-0 mb-3 mb-md-4">
                    <Row className="row-cols-1 row-cols-lg-3 justify-content-evenly align-items-center" style={{ minHeight: '400px'}}>

                        <Col>
                          <h6 className="pe-3 ps-4"><a className="text-reset text-decoration-underline" target="_blank" href={intl.formatMessage({id:`meta.links.redcross.link.${country}`, defaultMessage: "--"})}><FormattedMessage id={`meta.links.redcross.title.${country}`} /></a></h6>
                          <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.black.body.redcross" /></p>
                        </Col>

                        <Col>
                          <h6 className="pe-3 ps-4"><a className="text-reset text-decoration-underline" target="_blank" href={intl.formatMessage({id:`meta.links.response.link.${country}`, defaultMessage: "--"})}><FormattedMessage id={`meta.links.response.title.${country}`} /></a></h6>
                          <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.black.body.response" /></p>
                        </Col>

                        <Col>
                          <h6 className="pe-3 ps-4"><a className="text-reset text-decoration-underline" target="_blank" href={intl.formatMessage({id:`meta.links.weather.link.${country}`, defaultMessage: "--"})}><FormattedMessage id={`meta.links.weather.title.${country}`} /></a></h6>
                          <p className="ps-5 pe-3" style={{maxWidth: '330px'}}><FormattedMessage id="home.black.body.weather" /></p>
                        </Col>

                    </Row>

                    <small className="position-absolute bottom-0 start-50 translate-middle-x mb-2 text-center" style={{color: '#47474f', minWidth: '16rem'}}><FormattedMessage id="home.black.credit.1" /><a className="text-reset" target="_blank" href="https://country.is"><FormattedMessage id="home.black.credit.link" /></a></small>
                </div>





          </Card>


          <Card ref={divRef} className="rounded rounded-5 border-0 shadow m-0 mt-3" style={{backgroundColor: 'white', overflow: 'hidden'}}>
                <Card.Body className="p-3 p-md-4">

                  <div className="d-flex flex-wrap justify-content-between">

                    <Card.Title className="my-auto" style={{color: 'black'}}><FormattedMessage id="home.articles.title" /></Card.Title>


                    <div className="d-flex flex-wrap justify-content-end">

                      <Button variant={filter === 'all' ? "primary" : "outline-primary"} onClick={() => setFilter('all')} className="rounded-4 me-1 my-1 my-md-0"><FormattedMessage id="home.articles.buttons.all" /></Button>

                      <Button variant={filter === 'beg' ? "success" : "outline-success"} onClick={() => setFilter('beg')} className="rounded-4 mx-1 my-1 my-md-0"><FormattedMessage id="home.articles.buttons.beg" /></Button>

                      <Button variant={filter === 'adv' ? "warning" : "outline-warning"} onClick={() => setFilter('adv')} className="rounded-4 mx-1 my-1 my-md-0"><FormattedMessage id="home.articles.buttons.adv" /></Button>

                      <Button variant={filter === 'com' ? "danger" : "outline-danger"} onClick={() => setFilter('com')} className="rounded-4 ms-1 my-1 my-md-0"><FormattedMessage id="home.articles.buttons.com" /></Button>

                    </div>
                  </div>

                </Card.Body>

          </Card>

          <Row className="mt-3">
            {filteredArticles && filteredArticles.map((article, index) => (
              <Col md={12} lg={4}>

                <Card className={`rounded rounded-5 shadow mt-2 border-start border-4 border-top-0 border-end-0 border-bottom-0 ${
                        article.difficulty === 'beg' ? 'border-success' :
                        article.difficulty === 'adv' ? 'border-warning' :
                        article.difficulty === 'com' ? 'border-danger' : ''
                    }`}  style={{backgroundColor: 'white', overflow: 'hidden'}}>
                    <Card.Body className="p-3">

                      <Card.Title className="mb-2" style={{color: 'black'}}>
                                        {locale === 'es' ? article.titleEN :
                                          (locale === 'ru' ? article.titleRU :
                                            article.titleEN)}
                      </Card.Title>

                        <Button variant="outline-primary mt-auto rounded-4" onClick={() => navigate(`/article/${article.id}`)} ><FormattedMessage id="home.articles.read" /> <FontAwesomeIcon icon={faChevronRight} className=""/></Button>{' '}
                    </Card.Body>

                </Card>
              </Col>
            ))}
          </Row>

        </>


      </Container>



      </div>

      <div className="shadow py-5" style={{minHeight: "200px", color: 'white', backgroundColor: 'black'}} >

        <Container>
            <Row className="row-cols-">
                <Col md={12} lg={3} className="my-3">

                  <h6><FormattedMessage id="home.footer.licence.title" /></h6>

                  <Stack direction="vertical" style={{maxWidth: '300px'}}>
                    <small className="mb-2"><FormattedMessage id="home.footer.licence.1" /></small>
                    <small className="mb-2"><a className="text-reset" target="_blank" href="https://choosealicense.com/licenses/agpl-3.0/"><FormattedMessage id="home.footer.licence.link.1" /></a></small>
                    <small className="mb-2"><a className="text-reset" target="_blank" href="https://gitlab.com/Pixselious/bugout"><FormattedMessage id="home.footer.licence.link.2" /></a></small>
                  </Stack>

                </Col>
                <Col md={12} lg={3} className="my-3">

                  <h6><FormattedMessage id="home.footer.sources.title" /></h6>

                  <Stack direction="vertical" style={{maxWidth: '300px'}}>
                    <small className="mb-2"><FormattedMessage id="home.footer.sources.1" /></small>
                    <small className="mb-2"><FormattedMessage id="home.footer.sources.2" /></small>
                  </Stack>

                </Col>
                <Col md={12} lg={3} className="my-3">

                  <h6><FormattedMessage id="home.footer.privacy.title" /></h6>

                  <Stack direction="vertical" style={{maxWidth: '300px'}}>
                    <small className="mb-2"><FormattedMessage id="home.footer.privacy.1" /></small>
                    <small className="mb-2"><FormattedMessage id="home.footer.privacy.2" /></small>
                    <small className="mb-2"><FormattedMessage id="home.footer.privacy.3" /></small>
                    <small className="mb-2"><FormattedMessage id="home.footer.privacy.4" /></small>
                  </Stack>

                </Col>
                <Col md={12} lg={3} className="my-3">

                  <h6><FormattedMessage id="home.footer.credits.title" /></h6>

                  <Stack direction="vertical" style={{maxWidth: '300px'}}>
                    <small className="mb-2"><FormattedMessage id="home.footer.credits.1" /></small>
                    <small className="mb-2"><FormattedMessage id="home.footer.credits.2.1" /><a className="text-reset" target="_blank" href="https://youtube.com/playlist?list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p">Django</a><FormattedMessage id="home.footer.credits.2.2" /><a className="text-reset" target="_blank" href="https://youtube.com/playlist?list=PLzMcBGfZo4-kCLWnGmK0jUBmGLaJxvi4j">React + Django</a><FormattedMessage id="home.footer.credits.2.3" /></small>

                    <small className="mb-2"><FormattedMessage id="home.footer.credits.3" /></small>
                    <small className="mb-2"><FormattedMessage id="home.footer.credits.4" /></small>
                  </Stack>

                </Col>
            </Row>
        </Container>

      </div>



      </div>
  );
}
