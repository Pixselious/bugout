import React, { Component, useState, useEffect } from "react";
import { Button, Card, Container, Row, Col, Stack, Modal, CloseButton} from 'react-bootstrap';
import { useNavigate, useLocation } from "react-router-dom";
import * as Icon from 'react-bootstrap-icons';
import { FormattedMessage, useIntl } from "react-intl";



export default function ListExportPage({locale}) {

    const location = useLocation();
    const navigate = useNavigate();
    const intl = useIntl();

    const toGenerate = () => {
        navigate('/generate');
    }   

    const toHome = () => {
        navigate('/');
    } 

    function handleDownloadTxt() {
        setIsSaved(true);

        const element = document.createElement("a");
        const file = new Blob([converted.plaintext], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = "Packing_list.txt";
        document.body.appendChild(element); // Required for this to work in FireFox
        element.click();
    };

    function handleCopy() {
        setIsSaved(true);

        navigator.clipboard.writeText(converted.plaintext);
    }

    const [converted, setConverted] = useState(null);
    const [isSaved, setIsSaved] = useState(false);

    const [showSaveModal, setShowSaveModal] = useState(false);

    useEffect(() => {

        function convert() {

            var cache = JSON.parse(JSON.stringify(location.state.exportData));

            var output = {
                plaintext: '',
                html: '',
                pdf: null
            };

            Object.entries(cache).forEach(([key, value]) => { // Iterate over categories

                // Append string with category name (key)
                output.plaintext += key + '\n\n';

                for (let item = 0; item < value.length; item++) { // Iterate over listItems (values)

                    // Append string with listItem's selected option  
                    
                    output.plaintext += '\t' + value[item]['optionName'] + '\n';

                    if (value[item]['optionDescription'] != null) {
                        output.plaintext += '\t\t' + value[item]['optionDescription'] + '\n';
                    }

                };

                output.plaintext += '\n\n';
                 
            });


            // Append string with date and website 
            output.plaintext += "\n" + intl.formatMessage({id: 'export.internal.1'});
            output.plaintext += "\n" + intl.formatMessage({id: 'export.internal.2'});


            setConverted(output);
            
        };

        if(converted == null && location.state != null){
            convert();
        };

    });

    return( 
        <>

        { location.state == null &&

            <Container >

                <Row className="p-3 justify-content-center align-items-center" style={{height: "100vh"}}>

                    <Card className="rounded rounded-4 border-0 shadow p-3 my-3" style={{maxWidth: "50rem"}}>
                        <div className="">
                            <h1 className="mb-3"><FormattedMessage id='export.error.title' /></h1>
                            <h4 className="mb-5"><FormattedMessage id='export.error.subtitle.1' /><a href="#" onClick={ () => {toGenerate() }}><FormattedMessage id='export.error.subtitle.link' /></a><FormattedMessage id='export.error.subtitle.2' /></h4>
                            <p className="mb-0" style={{maxWidth: "25rem"}}><FormattedMessage id='export.error.notice' /></p>
                        </div>
                    </Card>

                </Row>

            </Container>

        }

        { location.state != null &&

            <Container>

                <Row className="p-3 justify-content-center align-items-center" style={{height: "100vh"}}>

                    <Card className="rounded rounded-4 border-0 shadow p-0 m-3" style={{maxWidth: "50rem"}}>

                        <h3 className="m-3 mb-0"><FormattedMessage id='export.title.1' /> { converted == null ? <FormattedMessage id='export.title.generating' /> : <FormattedMessage id='export.title.ready' /> }</h3>


                        { converted != null &&
                        
                            <Row className="m-2 row-cols-auto">
                                <Col className="m-0 p-0">
                                    <Button variant="outline-primary" className="rounded-4 m-2" onClick={ () => {handleCopy()}}><FormattedMessage id='export.button.copy' /> <Icon.Files /></Button>
                                </Col>
                                <Col className="m-0 p-0">
                                    <Button variant="outline-primary" className="rounded-4 m-2" onClick={ () => {handleDownloadTxt()}}><FormattedMessage id='export.button.download' /> .txt <Icon.FileEarmarkText /></Button>
                                </Col>
                            </Row>

                        }

                        <Card className="rounded-4 bg-light border-gray shadow p-0 m-3 mt-0" style={{}}>
                            <pre className="m-0 p-3 overflow-scroll lh-sm" style={{maxHeight: "40vh"}}>{ converted == null ? '' : converted.plaintext }</pre> 
                        </Card>

                        <p className="px-3 pt-3"><FormattedMessage id='export.text.2' /></p>

                        <Button className="mx-3 mb-3 rounded-4" variant={isSaved ? "primary" : "outline-secondary"} onClick={ () => { isSaved ? toHome() : setShowSaveModal(true)}}><FormattedMessage id='export.button.continue' /></Button>

                        <Modal show={showSaveModal} onHide={() => {setShowSaveModal(false)}} centered>

                            <Stack direction="horizontal">
                            
                                <p className="m-auto"><FormattedMessage id='export.modal' /></p>
                                <CloseButton className="p-3 ms-auto" onClick={() => setShowSaveModal(false)} />

                            </Stack>


                        </Modal>

                        

                    </Card>

                </Row>

            </Container>

        }
            
        </>
    );
}