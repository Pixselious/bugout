import React, { useState, useEffect } from "react";
import { Container, Form, Row, Col, Card, Button, Stack, Accordion, Modal } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { FormattedMessage, useIntl } from 'react-intl';
import * as Icon from 'react-bootstrap-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarDays, faCloudSun, faSnowflake, faSun } from "@fortawesome/free-solid-svg-icons";


//These variables are here because React's setState does not update immedeatley and messes everything up
var liveTotalWeight = 0.0 
var lastListItemName = ''

function ListGeneratorPage({locale}) {

    let navigate = useNavigate(); 

    function routeChange(exportData) { 
        console.log('redirecting...')
        navigate(`/generator`, {state: {exportData: exportData}});
    };      


    // Must match API model

    const categories = ['SAC', 'FAW', 'TMP', 'HFA', 'IAE', 'OTH', 'ADD'];

/*
    const categories = [
        { name: 'Shelter and clothing', short: 'SAC' },
        { name: 'Food and water', short: 'FAW' },
        { name: 'Temperature management', short: 'TMP' },
        { name: 'Health and first aid', short: 'HFA' },
        { name: 'Information and electronics', short: 'IAE' },
        { name: 'Other gear', short: 'OTH' },
        { name: 'Add-ons', short: 'ADD' },
    ]
*/
    const intl = useIntl();

    var initLocale = locale

    const [durationSelected, setDurationSelected] = useState(3);
    const [climateSelected, setClimateSelected] = useState('B');
    const [weightUnit, setWeightUnit] = useState("kg");
    const handleWeightUnit = (e) => setWeightUnit(e.target.value);
    const [weightNum, setWeightNum] = useState(0.0);
    const handleWeightNum = (e) => setWeightNum(e.target.value);

    const [isConfigurating, setisConfigurating] = useState(true);
    const [data, setData] = useState(null);
    const [cache, setCache] = useState(null);
    const [checked, setChecked] = useState(false);

    const [choices, setChoices] = useState(new Map());

    // We need to have two separate values for weight because React's setState does not update immedeatley and messes up the math.
    const [totalWeight, setTotalWeight] = useState(0.0); //For UI. Updated after all math is done to liveTotalWeight (declared outside of the ListGeneratorPage() func).

    const [showResetModal, setShowResetModal] = useState(false);


    function handleModalYes() {
        handleConfigurating();
        setShowResetModal(false);
    };

        function handleModalNo() {
        setShowResetModal(false);
    };



    function setLastListItemName(newName) {
        lastListItemName = newName;
    }



    function initChoice(listItemId, selectedId, newWeight) { //Called on render of accordeon components. Should be done only once to preserve existing user data.

        if (!choices.has(listItemId)) { //Make sure we are making a new "choice"

            setChoices(new Map(choices.set(listItemId, selectedId)))
            
            if (selectedId != null) {
                liveTotalWeight += newWeight;
                setTotalWeight(liveTotalWeight);
            }
        }

    }



    function handleChoiceChange(listItemId, selectedId, newWeight, allowNoSelection) { //Called on button press.

        // Subtract current choice's weight from totalWeight, then add new choice's weight to totalWeight


        if(choices.get(listItemId) != null) { // if something is already chosen

            // ok, here is this monstrosity explained: from data we get a matching listItem(), then from its options we get the one that is currntly chosen (choices.get), then we get its weight and subtract it from the current total. 
            //My brain is almost melting writing this, and the whole "store only ids" thing is probably not saving that much memory.
            liveTotalWeight -= data.find(listItem => listItem.id === listItemId).options.find(option => option.id === choices.get(listItemId)).weight;

        }

        if(allowNoSelection == false) {

            setChoices(new Map(choices.set(listItemId, selectedId)))
            liveTotalWeight += newWeight;

        } else {
            if(choices.get(listItemId) == selectedId) { // If user clicks on selected option, remove it from the list

                setChoices(new Map(choices.set(listItemId, null)))

            } else { // otherwise treat like a nomal radio button

                setChoices(new Map(choices.set(listItemId, selectedId)))
                liveTotalWeight += newWeight;
            }
        }

        setTotalWeight(liveTotalWeight);
    };





    function handleConfigurating() {

        if(isConfigurating == true){// If done selectimg filters, apply them to cached data and save in a new variable to be used in UI

            var climateLetter = climateSelected

            setChoices(new Map()); // Ensure that choices are reset to defaults (the defaults will be applied on render of <Accordeon/>)
            liveTotalWeight = 0 // Reset the total weight
            setTotalWeight(liveTotalWeight) // Sync UI variable to the source of truth

            var tmp = JSON.parse(JSON.stringify(cache));

            for (let i = 0; i < tmp.length; i++) { // Iterate over listItems
                var options = tmp[i].options

                for (let s = 0; s < options.length;) { // Iterate over and filter listItem.options 
                    if ((options[s].climate == climateLetter || options[s].climate == 'A' ) && (options[s].days == durationSelected || options[s].days == 0)){
                        s++;
                    }else{
                        options.splice(s, 1);
                    }
                }

            }

            setData(tmp)

        };

        setisConfigurating(!isConfigurating);

    };



    function handleGenerate() { //Called once, shows "r u sure" modal, packs up user's choices and redirects to saving page


        // 1 iterate over keys of choices 2 find corresponding listItem, get its category 3 find this key's values' corresponding option and get its data 4 package this data and add to dict { (cat: [(data to be displayed), ...]), ...}

        var exportData = {};
        
        /*{ data in here should be ready to be dislayed to the user, so no ids or codenames
            'Shelter and clothing': [{listItemName: 'name', content: 'Lorem ipsum'}, ... ],
            'Food and water': [],
            'Temperature management': [],
            'Health and first aid': [],
            'Information and electronics': [],
            'Other gear': [],
            'Add-ons': [], 
        }*/

        for (var i = 0; i < categories.length; i++) {
            exportData[intl.formatMessage({id: `meta.category.${categories[i]}`})] = [] 
        }

        for (const [key, value] of choices){

            if(value != null){ 

            var tmpListItem = data.find( listItem => listItem.id === key );
            var tmpOption = tmpListItem.options.find( option => option.id === value );

            // Get any data we want do export
            var bundle = {
                listItemName: intl.formatMessage({id:`meta.db.listitem.${tmpListItem.id}`, defaultMessage: " "}), 
                optionName: intl.formatMessage({id:`meta.db.name.${tmpOption.id}`, defaultMessage: " "}),
                optionDescription: intl.formatMessage({id:`meta.db.export.${tmpListItem.id}`, defaultMessage: " "}) == '+' ? intl.formatMessage({id:`meta.db.description.${tmpOption.id}`, defaultMessage: " "}) : intl.formatMessage({id:`meta.db.export.${tmpOption.id}`, defaultMessage: " "}),
            };

            // Add the data bundle to a corresponding array in the exportData object 
            exportData[intl.formatMessage({id: `meta.category.${tmpListItem.category}`})].push(bundle);

            };

        };

        navigate(`/export`, {state: {exportData: exportData}});

    };



    function toJson(data) {
        return data.json();
    };



    useEffect(() => {

        function getData() {
            fetch('/api/get-list')
                    .then( (response) => toJson(response) )
                    .then( (formatted) => {
                        setCache(formatted)
                    }).catch((e) => console.log(e))
        };

        if(cache == null){
            getData();
        };

    });



    function afterSubmission(event) {
        event.preventDefault();
    };




    return (
    <Container>

    <Row style={isConfigurating ? {height: "100vh"} : {}} className="p-2 justify-content-center align-items-center">

        <Card className="rounded rounded-4 border-0 shadow px-0 my-3" style={{maxWidth: "50rem"}}>

            { isConfigurating &&
                <Card.Body className="px-0 pb-0">

                <h3 className="mx-3 mb-0"><FormattedMessage id="generator.duration.title" /></h3>
                <p className="lh-sm mx-3"><small style={{color: "gray"}}><FormattedMessage id='generator.duration.description' /></small></p>

                <div className="p-2 pb-4 " style={{alignItems: "stretch", display: "flex", flexDirection: "row", flexWrap: "nowrap", overflowY: "hidden", overflowX: "auto"}}>
                        
                    <a onClick={() => setDurationSelected(3)}>
                        <Card className={durationSelected == 3 ? "rounded rounded-4 border-primary shadow my-0 mx-2" : "rounded rounded-4 border-light shadow my-0 mx-2"} style={{ width: '10rem', flexGrow: 0, flexShrink: 0}}>
                            <Card.Body>
                                <Card.Title><FormattedMessage id="generator.duration.1.h" /></Card.Title>
                                <Card.Text><FormattedMessage id="generator.duration.1.p" /></Card.Text>
                            </Card.Body>
                        </Card>
                    </a>
                    {/*
                    <a onClick={() => setDurationSelected(7)}>
                        <Card className={durationSelected == 7 ? "rounded rounded-4 border-primary shadow my-0 mx-2" : "rounded rounded-4 border-light shadow my-0 mx-2"} style={{ width: '10rem', flexGrow: 0, flexShrink: 0}}>
                            <Card.Body>
                                <Card.Title><FormattedMessage id="generator.duration.2.h" /></Card.Title>
                                <Card.Text><FormattedMessage id="generator.duration.2.p" /></Card.Text>
                            </Card.Body>
                        </Card>
                    </a>

                    <a onClick={() => setDurationSelected(14)}>
                        <Card className={durationSelected == 14 ? "rounded rounded-4 border-primary shadow my-0 mx-2" : "rounded rounded-4 border-light shadow my-0 mx-2"} style={{ width: '10rem', flexGrow: 0, flexShrink: 0}}>
                            <Card.Body>
                                <Card.Title><FormattedMessage id="generator.duration.3.h" /></Card.Title>
                                <Card.Text><FormattedMessage id="generator.duration.3.p" /></Card.Text>
                            </Card.Body>
                        </Card>
                    </a>
                    */}
                </div>



                <h3 className="mx-3 mb-0"><FormattedMessage id="generator.climate.title" /></h3>
                <p className="lh-sm mx-3"><small style={{color: "gray"}}><FormattedMessage id='generator.climate.description' /></small></p>

                <div className="p-2 pb-4 " style={{alignItems: "stretch", display: "flex", flexDirection: "row", flexWrap: "nowrap", overflowY: "hidden", overflowX: "auto"}}>
                        
                    <a onClick={() => setClimateSelected('B')}>
                        <Card className={climateSelected == 'B' ? "rounded rounded-4 border-primary shadow my-0 mx-2" : "rounded rounded-4 border-light shadow my-0 mx-2"} style={{ width: '10rem', flexGrow: 0, flexShrink: 0}}>
                            <Card.Body>
                                <Card.Title><FormattedMessage id="generator.climate.B" /></Card.Title>
                                <Card.Text><FormattedMessage id="generator.climate.1.p" /></Card.Text>
                            </Card.Body>
                        </Card>
                    </a>

                    <a onClick={() => setClimateSelected('H')}>
                        <Card className={climateSelected == 'H' ? "rounded rounded-4 border-primary shadow my-0 mx-2" : "rounded rounded-4 border-light shadow my-0 mx-2"} style={{ width: '18rem', flexGrow: 0, flexShrink: 0}}>
                            <Card.Body>
                                <Card.Title><FormattedMessage id="generator.climate.H" /></Card.Title>
                                <Card.Text><FormattedMessage id="generator.climate.2.p" /></Card.Text>
                            </Card.Body>
                        </Card>
                    </a>

                    <a onClick={() => setClimateSelected('C')}>
                        <Card className={climateSelected == 'C' ? "rounded rounded-4 border-primary shadow my-0 mx-2" : "rounded rounded-4 border-light shadow my-0 mx-2"} style={{ width: '18rem', flexGrow: 0, flexShrink: 0}}>
                            <Card.Body>
                                <Card.Title><FormattedMessage id="generator.climate.C" /></Card.Title>
                                <Card.Text><FormattedMessage id="generator.climate.3.p" /></Card.Text>
                            </Card.Body>
                        </Card>
                    </a>
                        
                </div>



                <h3 className="mx-3 mb-0"><FormattedMessage id="generator.weight.title" /></h3>
                <p className="lh-sm mx-3"><small style={{color: "gray"}}>(<FormattedMessage id='generator.optional'/>) <FormattedMessage id='generator.weight.description' /></small></p>

                <Form className="mx-3 mt-2 mb-4" onSubmit={afterSubmission}>
                    <Row >

                        <Col className="pe-3"xs="auto">
                            
                            <Form.Control value={weightNum} onChange={handleWeightNum} type="number" style={{width: 80}}/>
                        
                        </Col>

                        <Col className="ps-0" xs="auto">

                            <Form.Select value={weightUnit} onChange={handleWeightUnit}>
                                <option value="kg">kg</option>
                                <option value="lbs">lbs</option>
                            </Form.Select>
                        
                        </Col>

                    </Row>

                </Form>

                </Card.Body>
            }
            <Stack direction="horizontal">

                { !isConfigurating &&
                    <>

                        <h6 className="ms-3 me-1 me-md-2 my-0"><FontAwesomeIcon icon={faCalendarDays} /> { durationSelected }</h6>

                        <h6 className="mx-1 mx-md-2 my-0">
                            { climateSelected == "B" && <FontAwesomeIcon icon={faCloudSun} />}
                            { climateSelected == "C" && <FontAwesomeIcon icon={faSnowflake} />}
                            { climateSelected == "H" && <FontAwesomeIcon icon={faSun} />}
                        </h6>


                        {weightNum != 0.0 
                            ? <h6 className="mx-1 mx-md-2 my-0"><a className={ totalWeight + 0.5 >= ( weightUnit == 'kg' ? Math.round(weightNum / 100 * 20) : Math.round(weightNum / 100 * 20 / 2.205)) ? "text-decoration-none text-danger" : "text-decoration-none text-body"} >{ weightUnit == 'kg' ? totalWeight.toFixed(1) : (totalWeight * 2.205).toFixed(1) }</a>/{Math.round(weightNum / 100 * 20)} { weightUnit }</h6>
                            : <h6 className="mx-1 mx-md-2 my-0">{ weightUnit == 'kg' ? totalWeight.toFixed(1) : (totalWeight * 2.205).toFixed(1) } { weightUnit }</h6>
                        }

                    </>   // lbs convertion { weightUnit == 'kg' ? Math.round(weightNum / 100 * 20) : Math.round(weightNum / 100 * 20 * 2.205) } 
                }

                { isConfigurating &&
                    <small className="lh-sm mx-3"><FormattedMessage id="generator.privacy" /></small>
                }

                <Button variant={isConfigurating ? "outline-primary" : "outline-danger"} onClick={() => isConfigurating ? handleConfigurating() : setShowResetModal(true)} className={isConfigurating ? "m-2 ms-auto rounded-4" : "m-2 ms-auto rounded-4"}>{isConfigurating ? <FormattedMessage id={`generator.bar.button.next`} /> : <FormattedMessage id={`generator.bar.button.reset`} />}</Button>

                <Modal show={showResetModal} onHide={() => {setShowResetModal(false)}} centered>

                    <Modal.Body>
                        <h5><FormattedMessage id={`generator.bar.modal.title`} /></h5>

                        <Stack direction="horizontal">

                            <Button variant="outline-danger" onClick={handleModalYes} className="me-3 rounded-4"><FormattedMessage id={`generator.bar.modal.yes`} /></Button>

                            <Button variant="primary" onClick={handleModalNo} className="ms-auto rounded-4"><FormattedMessage id={`generator.bar.modal.no`} /></Button>

                        </Stack>

                    </Modal.Body>

                </Modal>

            </Stack>

        </Card>

        { !isConfigurating && 

            <Card className="rounded rounded-4 border-0 shadow bg-light px-0 my-3" style={{maxWidth: "50rem"}}>

            <h4 className="fw-normal m-3 mb-0"><FormattedMessage id={`generator.card.title`} /></h4>
            <p className="lh-sm mx-3"><small style={{color: "gray"}}><FormattedMessage id='generator.card.subtitle' /></small></p>



            <Card.Body className="p-0">

                <Accordion className="borderless" defaultActiveKey={['SAC']} flush style={{textColor: 'white'}}>

                        {categories.map( category => ( 

                            <Accordion.Item key={category} eventKey={category}>
                                <Accordion.Header><FormattedMessage id={`meta.category.${category}`} /></Accordion.Header>
                                <Accordion.Body className="px-0 pb-0">
                                    
                                    {data.filter(i => i.category == category).map( listItem => (
                                        <div key={listItem.id}>
                                        { listItem.options.length != 0 &&

                                        <>
                                            {/*If this is a new listItem, show the name*/}
                                            <h3 className="mx-3 mb-0">{intl.formatMessage({id:`meta.db.listitem.${listItem.id}`, defaultMessage: " ",}) != lastListItemName && intl.formatMessage({id:`meta.db.listitem.${listItem.id}`, defaultMessage: " ",})}</h3> 
                                            
                                            {/*If this listItem is optional, show the 'optional' text*/}
                                            {listItem.optional && <small className="mx-3" style={{color: 'gray'}}>(<FormattedMessage id='generator.optional'/>)</small>} 

                                            {/*If this is an old listItem and it is not optional, show a line instead of the name or 'optional' text*/}
                                            {intl.formatMessage({id:`meta.db.listitem.${listItem.id}`, defaultMessage: " ",}) == lastListItemName && !listItem.optional && <div className="mx-3 mb-3 mt-0" style={{width: '100px', height: '1px', backgroundColor: 'gray'}}></div> }

                                            { setLastListItemName(intl.formatMessage({id:`meta.db.listitem.${listItem.id}`, defaultMessage: " ",})) /*Setting the current name for the next listItem to check*/ }

                                            {
                                                { 
                                                    1: <> {/*If there is just one option and it is optional (can be unselected)*/}
                                                        

                                                        { listItem.optional && <>

                                                            { listItem.options[0].defaultSelected 
                                                                ? initChoice(listItem.id, listItem.options[0].id, listItem.options[0].weight)
                                                                : initChoice(listItem.id, null)
                                                            }

                                                            <a onClick={() => handleChoiceChange(listItem.id, listItem.options[0].id, listItem.options[0].weight, true)} key={listItem.options[0].id}>
                                                            <Card className={choices.get(listItem.id) == listItem.options[0].id ? "rounded rounded-4 border-primary shadow m-2 mx-3 mb-4" : "rounded rounded-4 border-light shadow m-2 mx-3 mb-4"} style={{ width: '16rem', flexGrow: 0, flexShrink: 0}}>
                                                                <Card.Body>
                                                                    <Card.Title className={ intl.formatMessage({id:`meta.db.description.${listItem.options[0].id}`, defaultMessage: " ",})  == " " && "mb-0"}>{intl.formatMessage({id:`meta.db.name.${listItem.options[0].id}`, defaultMessage: " "})}</Card.Title>
                                                                    <Stack direction="vertical">
                                                                        <p className="mb-1">{intl.formatMessage({id:`meta.db.description.${listItem.options[0].id}`, defaultMessage: " "}) }</p>
                                                                        <small className="mb-0" style={{color: "gray"}}>{weightUnit == 'kg' ? listItem.options[0].weight : Math.round(listItem.options[0].weight * 2.205) } {weightUnit}</small>
                                                                    </Stack>
                                                                </Card.Body>
                                                            </Card>
                                                            </a>

                                                        </>}

                                                        { !listItem.optional && <> {/*If there is just one option and it is NOT optional (can not be unselected)*/}

                                                            {initChoice(listItem.id, listItem.options[0].id, listItem.options[0].weight)}
                                                        

                                                            <Card className={"rounded rounded-4 border-secondary shadow m-2 mx-3 mb-4"} style={{ width: '16rem', flexGrow: 0, flexShrink: 0}}>
                                                                <Card.Body>
                                                                    <Card.Title className={ intl.formatMessage({id:`meta.db.description.${listItem.options[0].id}`, defaultMessage: " "}) == " " && "mb-0"}>{intl.formatMessage({id:`meta.db.name.${listItem.options[0].id}`, defaultMessage: " "})}</Card.Title>
                                                                    <Stack direction="vertical">
                                                                        <p className="mb-1">{intl.formatMessage({id:`meta.db.description.${listItem.options[0].id}`, defaultMessage: " "})}</p>
                                                                        <small className="mb-0" style={{color: "gray"}}>{weightUnit == 'kg' ? listItem.options[0].weight : Math.round(listItem.options[0].weight * 2.205) } {weightUnit}</small>
                                                                    </Stack>
                                                                </Card.Body>
                                                            </Card>

                                                        </>}

                                                    
                                                </>}[listItem.options.length]  || <div>
                                                    { listItem.optional && initChoice(listItem.id, null)}
                                                    { !listItem.optional && initChoice(listItem.id, listItem.options[0].id)}
                                                    
                                                    {/*If there is multiple options*/}

                                                    <div className="p-2 pb-4 " style={{alignItems: "stretch", display: "flex", flexDirection: "row", flexWrap: "nowrap", overflowY: "hidden", overflowX: "auto"}}>
                                                            
                                                        {listItem.options.map( option => (

                                                            <a onClick={() => handleChoiceChange(listItem.id, option.id, option.weight, listItem.optional)} key={option.id}>
                                                            <Card className={choices.get(listItem.id) == option.id ? "rounded rounded-4 border-primary shadow my-0 mx-2" : "rounded rounded-4 border-light shadow my-0 mx-2"} style={{ width: '16rem', flexGrow: 0, flexShrink: 0}}>
                                                                <Card.Body>
                                                                    <Card.Title className={ intl.formatMessage({id:`meta.db.description.${option.id}`, defaultMessage: " "}) == null && "mb-0"}>{intl.formatMessage({id:`meta.db.name.${option.id}`, defaultMessage: " "})}</Card.Title>
                                                                    <Stack direction="vertical">
                                                                        <p className="mb-1">{option[`description${ initLocale.toUpperCase() }`]}</p>

                                                                        { intl.formatMessage({id:`meta.db.advantage.${option.id}`, defaultMessage: " "}) != " " && <small className="mb-0"> <Icon.PlusCircle /> {intl.formatMessage({id:`meta.db.advantage.${option.id}`, defaultMessage: " "})}</small> }

                                                                        { intl.formatMessage({id:`meta.db.tradeoff.${option.id}`, defaultMessage: " "}) != " " && 
                                                                        <small className="mb-1"> <Icon.DashCircle /> {intl.formatMessage({id:`meta.db.tradeoff.${option.id}`, defaultMessage: " "})}</small>}

                                                                        <small className="mb-0" style={{color: "grey"}}>{weightUnit == 'kg' ? option.weight : Math.round(option.weight * 2.205) } {weightUnit}</small>
                                                                    </Stack>
                                                                </Card.Body>
                                                            </Card>
                                                            </a>

                                                        ))}

                                                    </div> 

                                                </div>
                                            }
                                        
                                        </>

                                        }
                                        </div>
                                    ))}
                                    
                                    
                                </Accordion.Body>
                            </Accordion.Item>
                        

                        ))}

                        
                </Accordion>
        
            </Card.Body>
            <Stack direction="horizontal">
                
                <small className="m-2 ms-3 lh-sm"><FormattedMessage id={`generator.card.text`} /></small>
                <Button variant="outline-primary" className="m-2 ms-auto rounded-4" onClick={handleGenerate}><FormattedMessage id={`generator.card.button`} /></Button>
            </Stack>

            

            </Card> 
        }

    </Row>
    </Container>
  );
}

export default ListGeneratorPage; 