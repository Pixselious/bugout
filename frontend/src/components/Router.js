import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import HomePage from "./HomePage";
import ListGeneratorPage from "./ListGeneratorPage";
import ListExportPage from "./ListExportPage";
import ArticlePage from "./ArticlePage";

var locale

export default function App ({changeLocale, locale}) {



        return (
            <div>
                <Router>
                    <Routes>

                        <Route exact path="/" element={<HomePage changeLocale={changeLocale} locale={locale} />} />

                        <Route path="/generate" element={<ListGeneratorPage locale={locale} />} />

                        <Route path="/export" element={<ListExportPage locale={locale} />} />

                        <Route path="/article/:articleID" element={<ArticlePage locale={locale} />} />


                    </Routes>
                </Router>
            </div>
        );

}
