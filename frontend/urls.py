from django.urls import path
from .views import index

urlpatterns = [
    path('', index),
    path('generate', index),
    path('export', index),
    path('article/<int:article_id>', index),
]
